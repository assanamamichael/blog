from django.db import models
from django.contrib.auth import get_user_model
from django.urls import reverse
from tinymce.models import HTMLField
from django.contrib import admin

# Create your models here.
User = get_user_model()

STATUS_CHIOCES = [
    ('d', 'Draft'),
    ('p', 'Published'),
    ('w', 'Withdraw')

]

class Category(models.Model):
    title = models.CharField(max_length=100)

    def __str__(self):
        return self.title


class Author(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    profile_image = models.ImageField()

    def __str__(self):
        return self.user.username


class Post(models.Model):
    title = models.CharField(max_length=200)
    overview = HTMLField()
    timestamp = models.DateTimeField(auto_now_add=True)
    comment_count = models.IntegerField(default=0)
    author = models.ForeignKey(Author, on_delete=models.CASCADE)
    view_count = models.IntegerField(default=0)
    thumnail = models.ImageField()
    categories = models.ManyToManyField(Category)
    featured = models.BooleanField()
    status = models.CharField(max_length=1, choices=STATUS_CHIOCES)

    def __str__(self):
        return self.title

    def get_absolute_url(self):
        return reverse('post_detail', kwargs={
            'id': self.id
        })

#
# class Comment(models.Model):
#     post = models.ForeignKey(Post, on_delete=models.CASCADE, related_name='comments')
#     name = models.CharField(max_length=100)
#     email = models.EmailField()
#     body = models.TextField()
#     created_on = models.BooleanField(default=False)
#
#     class Mete:
#         ordering = [
#             'created_on'
#         ]
#
#     def __str__(self):
#         return 'Comment {} by {}'.format(self.body,self.name)
#
#
# class CommentAdmin(admin.ModelAdmin):
#     list_display = ('name', 'body', 'post', 'crated_on', 'active')
#     list_filter = ('active', 'created_on')
#     search_fields = ('name', 'email', 'body')
#     actions = ['approved_comment']
#
#     def approve_comments(self,request, queryset):
#         queryset.update(active=True)