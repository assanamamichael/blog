from django.contrib import admin
from .models import Post, Category, Author


# Register your models here.


def make_published(self, request, queryset):
    rows_updated = queryset.update(status='p')
    if rows_updated == 1:
        message_bit = "1 story was"
    else:
        message_bit = "%s stories were" % rows_updated
    self.message_user(request, "%s successfully marked as published." % message_bit)


make_published.short_description = "Mark selected stories as published"


class ArticleAdmin(admin.ModelAdmin):
    list_display = ['title', 'timestamp', 'status']
    ordering = ['title']
    actions = [
        'make_published',
        'make_withdrew'
    ]


admin.site.register(Post, ArticleAdmin)
admin.site.register(Category)
admin.site.register(Author)
