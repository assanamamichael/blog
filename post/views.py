from django.shortcuts import render, get_object_or_404
from .models import Post, Author
from subcribers.models import Signup
from django.core.paginator import Paginator
from django.db.models import Count, Q


def search(request):
    queryset = Post.objects.all()
    query = request.GET.get('q')
    if query:
        queryset = queryset.filter(
            Q(title__icontains=query) |
            Q(overview__icontains=query)
        ).distinct()
    context = {
        'queryset': queryset
    }
    return render(request, 'search_result.html', context)


def index(request):
    queryset = Post.objects.filter(featured=True)
    latest = Post.objects.order_by('-timestamp')[0:3]
    # handling subscriber's email
    if request.method == "POST":
        email = request.POST['email']
        new_signup = Signup()
        new_signup.email = email
        new_signup.save()
    context = {
        'object_list': queryset,
        'latest': latest
    }
    return render(request, "index.html", context)


def get_category_count():
    queryset = Post.objects.values('categories__title').annotate(Count('categories__title'))
    return queryset


def post(request, id):
    post_d = get_object_or_404(Post, id=id)
    # comment = CommentForm.objects.filter()
    # comments_c = post.comments.filter(active=True).order_by("-created_on")
    # new_comment = None
    # if request.method == "POST":
    #     comment_form = CommentForm(data=request.POST)
    #     if comment_form.is_valid():
    #         new_comment.post = post
    #         new_comment.save()
    #     else:
    #         comment_form = CommentForm()

    context = {
        'post_d': post_d,
        # 'comment': comment
        # 'comments_c': comments_c,
        # 'new_comment': new_comment,
        # 'comment_form': comment_form
    }
    return render(request, "post.html", context)


def blog(request):
    category_count = get_category_count()
    print(category_count)
    all_post = Post.objects.all()
    latest = Post.objects.order_by('-timestamp')[0:3]
    paginator = Paginator(all_post, 2)
    # page_var = 'page'
    page = request.GET.get('page')
    page_obj = paginator.get_page(page)
    # try:
    #     page_queryset = paginator.page(page)
    # except PageNotAnInteger:
    #     page_queryset = paginator.page(1)
    # except EmptyPage:
    #     page_queryset = paginator.page(paginator.num_pages)

    context = {
        # 'query': page_queryset,
        'latest': latest,
        'all_post': all_post,
        'page_obj': page_obj,
        'categories_count': category_count
    }
    return render(request, "blog.html", context)

